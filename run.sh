#!/bin/bash
# (C)opyright 2020 -- Billy's Program --


## Couleurs
magenta="\e[95m"
bleu="\e[94m"
cyan="\e[96m"
rouge="\e[91m"
orange="\e[33m"
jaune="\e[93m"
vertf="\e[32m"
vert="\e[92m"
blanc="\e[97m"


## Affectation du mot "pause"
pause(){
        local m="$@"
        echo "$m"
echo -e "$rouge"'Appuyer la touche [ENTER] pour continuer'
        read -p "" key
}

clear
echo -e "$rouge""
                    ######                        ###
                    #     # # #      #      #   # ###  ####
                    #     # # #      #       # #   #  #
		    ######  # #      #        #   #    ####
                    #     # # #      #        #            #
		    #     # # #      #        #       #    #
                    ######  # ###### ######   #        ####
"
echo "
                    ######
                    #     # #####   ####   ####  #####    ##   #    #
                    #     # #    # #    # #    # #    #  #  #  ##  ##
                    ######  #    # #    # #      #    # #    # # ## #
                    #       #####  #    # #  ### #####  ###### #    #
                    #       #   #  #    # #    # #   #  #    # #    #
		    #       #    #  ####   ####  #    # #    # #    # "
sleep 1
clear
echo -e "$orange""
 			   ▄   ████▄   ▄      ▄▄▄▄▄                                 
			     █  █   █    █    █     ▀▄                               
			█     █ █   █ █   █ ▄  ▀▀▀▀▄                                 
			 █    █ ▀████ █   █  ▀▄▄▄▄▀                                  
			  █  █        █▄ ▄█                                          
			   █▐          ▀▀▀                                           
			   ▐                                                         
	█ ▄▄  █▄▄▄▄ ▄███▄     ▄▄▄▄▄   ▄███▄      ▄     ▄▄▄▄▀ ▄███▄   
	█   █ █  ▄▀ █▀   ▀   █     ▀▄ █▀   ▀      █ ▀▀▀ █    █▀   ▀  
	█▀▀▀  █▀▀▌  ██▄▄   ▄  ▀▀▀▀▄   ██▄▄    ██   █    █    ██▄▄    
	█     █  █  █▄   ▄▀ ▀▄▄▄▄▀    █▄   ▄▀ █ █  █   █     █▄   ▄▀ 
	 █      █   ▀███▀             ▀███▀   █  █ █  ▀      ▀███▀   
	  ▀    ▀                              █   ██                 
"
sleep 1; clear
echo -e "$magenta""\e[3m""Check device RTL""$orange""\e[25m"
lsusb|grep DVB
echo -e "$jaune"
rtl_eeprom -d 0
echo -e "$bleu""
			  ▄████   ██████  ███▄ ▄███▓
			 ██▒ ▀█▒▒██    ▒ ▓██▒▀█▀ ██▒
			▒██░▄▄▄░░ ▓██▄   ▓██    ▓██░
			░▓█  ██▓  ▒   ██▒▒██    ▒██
			░▒▓███▀▒▒██████▒▒▒██▒   ░██▒
			 ░▒   ▒ ▒ ▒▓▒ ▒ ░░ ▒░   ░  ░
			  ░   ░ ░ ░▒  ░ ░░  ░      ░
			░ ░   ░ ░  ░  ░  ░      ░
			      ░       ░         ░

		  ██████  ███▄    █  ██▓  █████▒ █████▒▓█████  ██▀███
		▒██    ▒  ██ ▀█   █ ▓██▒▓██   ▒▓██   ▒ ▓█   ▀ ▓██ ▒ ██▒
		░ ▓██▄   ▓██  ▀█ ██▒▒██▒▒████ ░▒████ ░ ▒███   ▓██ ░▄█ ▒
		  ▒   ██▒▓██▒  ▐▌██▒░██░░▓█▒  ░░▓█▒  ░ ▒▓█  ▄ ▒██▀▀█▄
		▒██████▒▒▒██░   ▓██░░██░░▒█░   ░▒█░    ░▒████▒░██▓ ▒██▒
		▒ ▒▓▒ ▒ ░░ ▒░   ▒ ▒ ░▓   ▒ ░    ▒ ░    ░░ ▒░ ░░ ▒▓ ░▒▓░
		░ ░▒  ░ ░░ ░░   ░ ▒░ ▒ ░ ░      ░       ░ ░  ░  ░▒ ░ ▒░
		░  ░  ░     ░   ░ ░  ▒ ░ ░ ░    ░ ░       ░     ░░   ░
		      ░           ░  ░                    ░  ░   ░
"


# grgsm_scanner
echo -e "$magenta""\e[3m""grgsm_scanner""\e[25m"
echo -e "$vert""Faut-il lancer un scan ? [y/n]"
while :
do
	read input
	case $input in
		[y/Y])
			echo
			echo -e "$bleu""Scannage en cours"""$cyan""
			grgsm_scanner -v
			echo
			break
			;;

		[n/N])
			echo
			break
			;;
		*)
			echo -e "$vert""Faut-il lancer un scan ? [y/n]"
			;;
	esac
done


# grgsm_capture
echo -e "$magenta""\e[3m""grgsm_capture""\e[25m"
echo -e "$vert""Faut-il lancer une capture (BCCH) ? [y/n]"
while :
do
	read input
	case $input in
		[y/Y])
echo
			#Choix de la frequence
echo -e "$vert""Veuillez saisir la frequence a capturer"
read -e freq
echo
			#Choix de l'ARFCN
echo "Veuillez saisir le numero ARFCN"
read -e arfcn
echo
			#Lancement de la capture
echo -e "$orange""Capture de l'ARFCN ""$jaune""$arfcn""$orange" "sur la frequence ""$jaune""$freq"
echo -e "$orange""Generation du fichier ""$jaune""/tmp/$arfcn.cfile""$blanc"
xterm -fa 'Monospace' -fs 10 -geometry 84x15+0 -T watch_cfile -e "watch 'ls -lh /tmp/$arfcn.*'" & watch=$!
xterm -fa 'Monospace' -fs 08 -geometry 98x19+1000 -T capture -e "grgsm_capture -a $arfcn -c /tmp/$arfcn.cfile -v" & capture=$!
echo
echo -e "Attendez d'avoir asser de paquets, puis appuyez sur "$rouge"[ENTREE] "$blanc"pour lancer le decodage !"
read
kill ${watch} >/dev/null 2>&1
kill ${capture} >/dev/null 2>&1
echo -e "$orange""Informations du fichier cfile""$jaune"
ls -lh /tmp/$arfcn.cfile
echo

			# grgsm_decode
echo -e "$magenta""\e[3m""grgsm_decode_cfile""\e[25m"

			# grgsm_decode BCCH
echo -e "$orange""Channel mode: ""$jaune""BCCH (Non-combined C0)"
echo -e "$orange""Time Slot: ""$jaune""0"
echo -e "$vertf"
tshark -i lo -f "port 4729" -w /tmp/BCCH$arfcn.pcap & tshark=$!
sleep 2
xterm -fa 'Monospace' -fs 08 -geometry 90x20+1000 -T decode_BCCH -e "grgsm_decode -a $arfcn -c /tmp/$arfcn.cfile -m BCCH -t 0 -v"
kill ${tshark} >/dev/null 2>&1
tshark -PVx -r /tmp/BCCH$arfcn.pcap > /tmp/BCCH$arfcn.txt
echo

			# Infos of BCCH
echo -e "$magenta""\e[3m""Information du BCCH""\e[25m"
echo -e "$bleu""Signal Level""$cyan"
cat /tmp/BCCH$arfcn.txt|grep "Signal Level (dBm):"|uniq|sort -u
echo
echo -e "$bleu""International Subscriber Identity (IMSI)""$cyan"
cat /tmp/BCCH$arfcn.txt|grep "IMSI:"|awk {'print $NF'}|uniq|sort -u|paste -s
echo
echo -e "$bleu""Temporary Mobile Subscriber Identity (TMSI)""$cyan"
cat /tmp/BCCH$arfcn.txt|grep "TMSI/P-TMSI:"|awk {'print $NF'}|uniq|sort -u|paste -s
echo
echo -e "$bleu""SDCCH8 (StanD-alone Control CHannel)""$cyan"
cat /tmp/BCCH$arfcn.txt|grep "SDCCH/8 + SACCH/C8 or CBCH (SDCCH/8):"|uniq|sort -u
echo
echo -e "$bleu""Location Area Identification (LAI)""$cyan"
cat /tmp/BCCH$arfcn.txt|grep -A 3 "Location Area Identification (LAI) -"|sort -u
echo
echo -e "$bleu""Cell Identity""$cyan"
cat /tmp/BCCH$arfcn.txt|grep "Cell CI:"|uniq
echo
echo -e "$bleu""List of ARFCNs""$cyan"
cat /tmp/BCCH$arfcn.txt|grep "List of ARFCNs"|sort -u
echo
echo -e "$bleu""Time Slot""$cyan"
cat /tmp/BCCH$arfcn.txt | grep -A 2 "SDCCH/8"|grep -v "SDCCH\|Subchannel"|uniq|sort -u
echo


			# Choix du TimeSlot
echo -e "$vert""Veuillez saisir le numero du Timeslot"
read timeslot
echo


			# grgsm_decode SDCCH8
echo -e "$orange""Time Slot: ""$jaune""$timeslot""$vertf"
tshark -i lo -f "port 4729" -w /tmp/SDCCH8$arfcn-TS$timeslot.pcap & tshark=$!
sleep 2
xterm -fa 'Monospace' -fs 08 -geometry 90x20+1000 -T decode_SDCCH8 -e "grgsm_decode -a $arfcn -c /tmp/$arfcn.cfile -m SDCCH8 -t $timeslot -v"
kill ${tshark} >/dev/null 2>&1
tshark -PVx -r /tmp/SDCCH8$arfcn-TS$timeslot.pcap > /tmp/SDCCH8$arfcn-TS$timeslot.txt
			break
			;;


		[n/N])
			#Choix de la frequence
echo
echo -e "$vert""Veuillez saisir une frequence"
read -e freq
echo
			#Choix de l'ARFCN
echo "Veuillez saisir le numero ARFCN correspondant"
read -e arfcn
echo
			#Choix du timeslot
echo "Veuillez saisir le numero du Timeslot"
read -e timeslot
echo
			break
			;;
		*)
			echo -e "$vert""Faut-il lancer une capture ? [y/n]"
			;;
	esac
done


# -- MENU --
while :
do
repeated=$(cat /tmp/SDCCH8$arfcn-TS$timeslot.txt|grep "TMSI/P-TMSI:"|uniq -d|wc -l)
totaltmsi=$(cat /tmp/SDCCH8$arfcn-TS$timeslot.txt|grep "TMSI/P-TMSI:"|uniq|sort -u|wc -l)
clear
echo -e $bleu"
██╗███╗   ██╗███████╗ ██████╗ ██████╗ ███╗   ███╗ █████╗ ████████╗██╗ ██████╗ ███╗   ██╗███████╗
██║████╗  ██║██╔════╝██╔═══██╗██╔══██╗████╗ ████║██╔══██╗╚══██╔══╝██║██╔═══██╗████╗  ██║██╔════╝
██║██╔██╗ ██║█████╗  ██║   ██║██████╔╝██╔████╔██║███████║   ██║   ██║██║   ██║██╔██╗ ██║███████╗
██║██║╚██╗██║██╔══╝  ██║   ██║██╔══██╗██║╚██╔╝██║██╔══██║   ██║   ██║██║   ██║██║╚██╗██║╚════██║
██║██║ ╚████║██║     ╚██████╔╝██║  ██║██║ ╚═╝ ██║██║  ██║   ██║   ██║╚██████╔╝██║ ╚████║███████║
╚═╝╚═╝  ╚═══╝╚═╝      ╚═════╝ ╚═╝  ╚═╝╚═╝     ╚═╝╚═╝  ╚═╝   ╚═╝   ╚═╝ ╚═════╝ ╚═╝  ╚═══╝╚══════╝

         ██████╗  █████╗ ████████╗██╗  ██╗███████╗██████╗ ██╗███╗   ██╗ ██████╗
        ██╔════╝ ██╔══██╗╚══██╔══╝██║  ██║██╔════╝██╔══██╗██║████╗  ██║██╔════╝
        ██║  ███╗███████║   ██║   ███████║█████╗  ██████╔╝██║██╔██╗ ██║██║  ███╗
        ██║   ██║██╔══██║   ██║   ██╔══██║██╔══╝  ██╔══██╗██║██║╚██╗██║██║   ██║
        ╚██████╔╝██║  ██║   ██║   ██║  ██║███████╗██║  ██║██║██║ ╚████║╚██████╔╝
         ╚═════╝ ╚═╝  ╚═╝   ╚═╝   ╚═╝  ╚═╝╚══════╝╚═╝  ╚═╝╚═╝╚═╝  ╚═══╝ ╚═════╝  "
echo -e "$magenta""\e[3m""			ARFCN: $arfcn | Frequency: $freq.hertz""\e[25m""$cyan"
echo -e "$cyan"
echo " 	 L. Lexique"
echo
echo "	 1. Cell CI + LAI"
echo
echo "	 2. IMSI + MCC + MNC"
echo
echo "	 3. ARFCNs"
echo
echo "	 4. Algorithm identifier"
echo
echo "	 5. TMSI"
echo
echo "	10. Modifier le Timeslot et/ou relancer une capture SDCCH/8"
echo
echo -e '\E[37;44m'"\033[1m""[✔] Timeslot: $timeslot"
echo
echo -e '\E[37;44m'"\033[1m""[✔] TMSI Vulnerable: $repeated""\033[0m""$cyan"
echo
echo "	00. Attack"
echo
echo "	99. Exit"
echo
echo -e "$vert"

	echo -n "Quel est votre choix ? "
	read choix
	echo

	case $choix in


	00)
		echo
		echo -e "$orange""Time Slot: ""$jaune""$timeslot""$cyan"
		echo
		echo -e "$orange""TMSI CAPTURED: ""$jaune""$totaltmsi"
		sleep 1
		cat /tmp/SDCCH8$arfcn-TS$timeslot.txt|grep "TMSI/P-TMSI:"|awk {'print $NF'}|uniq|sort -u|paste -s
		echo
		echo -e "$orange""TMSI REPEATED: ""$jaune""$repeated"
		cat /tmp/SDCCH8$arfcn-TS$timeslot.txt|grep "TMSI/P-TMSI:"|uniq -d
		echo
		sleep 1		
		echo -e "$orange""TMSI GROUPING: ""$jaune"
		for i in `cat /tmp/SDCCH8$arfcn-TS$timeslot.txt|grep "TMSI/P-TMSI:"|uniq -d|awk {'print $NF'}`;
		do
		cat /tmp/SDCCH8$arfcn-TS$timeslot.txt|grep -c "TMSI/P-TMSI: $i"; cat /tmp/SDCCH8$arfcn-TS$timeslot.txt|grep "TMSI/P-TMSI: $i"; echo "-----"
		done
		echo
		echo -e "$orange""Select TMSI ! (Grouping at least 4)""$jaune"
		read tmsi
		echo
		echo -e "$orange""FRAMES FOUND !""$jaune"
		cat /tmp/SDCCH8$arfcn-TS$timeslot.txt|grep -B 43 "Mobile Identity - TMSI/P-TMSI ($tmsi)"|grep "GSM Frame Number"
		echo
		pause
		;;


	1)
		echo
		echo -e "$orange""Timeslot: $timeslot""$jaune"" - Cell Identity and LAI"
		cat /tmp/SDCCH8$arfcn-TS$timeslot.txt|grep -A 7 "Cell Identity - CI"|sort|uniq|grep -v "SACCH\|Identity"
		echo
		pause
		;;


	2)
		echo
		echo -e "$orange""Timeslot: $timeslot""$jaune"" - International Mobile Subcriber Identity (IMSI)"
		sleep 1		
		cat /tmp/SDCCH8$arfcn-TS$timeslot.txt|grep -A 2 "IMSI:"
		echo
		echo -e "$orange""Mobile Country Code:""$jaune";
		cat /tmp/SDCCH8$arfcn-TS$timeslot.txt|grep -A 2 "IMSI:"|sort|uniq|grep -v "IMSI\|MNC"
		echo
		echo -e "$orange""Mobile Network Code:""$jaune"
		cat /tmp/SDCCH8$arfcn-TS$timeslot.txt|grep -A 2 "IMSI:"|sort|uniq|grep -v "IMSI\|MCC"
		echo
		pause
		;;


	3)
		echo
		echo -e "$orange""Timeslot: $timeslot""$jaune"" - Absolute Radio Frequency Channel (ARFCN)"
		cat /tmp/SDCCH8$arfcn-TS$timeslot.txt|grep "List of ARFCNs"|sort -u
		echo
		pause
		;;


	4)
		echo
		echo -e "$orange""Timeslot: $timeslot""$jaune"" - Algorithm identifier (A5)"
		cat /tmp/SDCCH8$arfcn-TS$timeslot.txt|grep -A 2 "Cipher Mode Setting"|sort|uniq|grep -v "Start\|Mode"
		echo
		pause
		;;	


	5)
		echo
		echo -e "$orange""Timeslot: $timeslot""$jaune"" - Temporary Mobile Subcriber Identity (TMSI)"
		sleep 1
		cat /tmp/SDCCH8$arfcn-TS$timeslot.txt|grep "TMSI/P-TMSI:"|awk {'print $NF'}|uniq|sort -u|paste -s
		echo
		pause
		;;


	L)
echo -e "$orange""Communications GSM900 en Europe""$jaune"
echo "De la MS (Mobile Station) vers la BTS (Station de base) = lien montant"
echo "Entre 880 MHz et 915 MHz (915-880=35)"
echo
echo "De la BTS (Station de base) vers la MS Mobile Station) = lien descendant"
echo "Entre 925 MHz et 960 MHz (960-925=35)"
echo
echo "Chacune de ces 2 bandes de 35 MHz est divisée en 174 canaux (ARFCN) de 200 Khz (0,2 Mhz)"
echo "35 / 174 = 0,2"
echo
echo "Chacun de ces 174 canaux (ARFCN) sont numérotés de 0 à 125 et de 975 à 1023"
echo "(1023-975)+(125+1)=174 (+1 correspond au 0 qui compte pour 1...)"
echo
echo "Chaque canal (ARFCN) est divisé en 8 time slot (TS) d’environ 577 μs et sont numérotés de 0 à 7"
echo
echo "Les Time slot sont les canaux physiques qui sont utilisés pour transmettre la voix ou la signalisation"
echo
echo "Les Time slot (TS) appartiennent à trois catégories"
echo "- Traffic CHannels (TCHs) => TCH/F - TCH/H"
echo "- Dedicated Control CHannels (DCCHs) => SDCCH - FACCH - SACCH"
echo "- Common Control CHannels (CCCHs) => BCCH - AGCH - RACH - PCH - SCH - FCCH"
echo
pause
;;


	10)
		#Choix du timeslot
		echo
		echo -e "$orange""Liste des Timeslot (BCCH)""$jaune"
		cat /tmp/BCCH$arfcn.txt | grep -A 2 "SDCCH/8"|grep -v "SDCCH\|Subchannel"|uniq|sort -u
		echo
		echo -e "$vert""Veuillez saisir le numero du Timeslot"
		read -e timeslot
		echo
		echo -e "$vert""Faut-il relancer un scan (SDCCH/8) ? [y/n]"
while :
do
	read input
	case $input in
		[y/Y])
			# grgsm_decode SDCCH8
			echo
			echo -e "$orange""Channel mode: ""$jaune""SDCCH8 (Stand-alone control channel)"
			echo -e "$orange""Time Slot: ""$jaune""$timeslot"
			echo
			tshark -i lo -f "port 4729" -w /tmp/SDCCH8$arfcn-TS$timeslot.pcap & tshark=$!
			sleep 2
			xterm -fa 'Monospace' -fs 08 -geometry 90x20+1000 -T decode_SDCCH8 -e "grgsm_decode -a $arfcn -c /tmp/$arfcn.cfile -m SDCCH8 -t $timeslot -v"
			kill ${tshark} >/dev/null 2>&1
			tshark -PVx -r /tmp/SDCCH8$arfcn-TS$timeslot.pcap > /tmp/SDCCH8$arfcn-TS$timeslot.txt
			echo
			echo -e "$orange""Generation des fichiers: ""$jaune""/tmp/SDCCH8$arfcn-TS$timeslot.txt"
			echo
			pause
			break
			;;

		[n/N])
			break
			;;
		*)
			echo "Veuillez choisir ? [y/n] "
			;;
	esac
done
;;


	99)
		break
		;;


	*)
		;;
	



	esac
done